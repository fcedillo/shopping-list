//
//  HCShoppingListViewController.h
//  Shopping List
//
//  Created by Franco Cedillo on 6/7/13.
//  Copyright (c) 2013 HartasCuerdas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCEditItemViewController.h"

@interface HCShoppingListViewController : UITableViewController <HCEditItemViewControllerDelegate>

@end
