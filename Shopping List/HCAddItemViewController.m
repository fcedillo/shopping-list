//
//  HCAddItemViewController.m
//  Shopping List
//
//  Created by Franco Cedillo on 5/27/13.
//  Copyright (c) 2013 HartasCuerdas. All rights reserved.
//

#import "HCAddItemViewController.h"

@interface HCAddItemViewController ()

@end

@implementation HCAddItemViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancel:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)save:(id)sender {
    // Extract User Input
    NSString *name = [self.nameTextField text];
    float price = [[self.priceTextField text] floatValue];
    // Notify Delegate
    [self.delegate controller:self didSaveItemWithName:name andPrice:price];
    // Dismiss View Controller
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
