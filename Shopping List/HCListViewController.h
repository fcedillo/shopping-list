//
//  HCListViewController.h
//  Shopping List
//
//  Created by Franco Cedillo on 5/27/13.
//  Copyright (c) 2013 HartasCuerdas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCAddItemViewController.h"
#import "HCEditItemViewController.h"
@interface HCListViewController : UITableViewController <HCAddItemViewControllerDelegate, HCEditItemViewControllerDelegate>

@end
