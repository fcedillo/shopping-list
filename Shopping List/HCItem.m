//
//  HCItem.m
//  Shopping List
//
//  Created by Franco Cedillo on 5/27/13.
//  Copyright (c) 2013 HartasCuerdas. All rights reserved.
//

#import "HCItem.h"

@implementation HCItem

- (void) encodeWithCoder: (NSCoder *)coder {
    [coder encodeObject:self.uuid forKey:@"uuid"];
    [coder encodeObject:self.name forKey:@"name"];
    [coder encodeFloat:self.price forKey:@"price"];
    [coder encodeBool:self.inShoppingList forKey:@"inShoppingList"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self) {
        [self setUuid:[decoder decodeObjectForKey:@"uuid"]];
        [self setName:[decoder decodeObjectForKey:@"name"]];
        [self setPrice:[decoder decodeFloatForKey:@"price"]];
        [self setInShoppingList:[decoder decodeBoolForKey:@"inShoppingList"]];
    }
    return self;
}

+ (HCItem *)createItemWithName:(NSString *)name andPrice:(float)price {
    // Initialize Item
    HCItem *item = [[HCItem alloc] init];
    // Configure Item
    [item setName:name];
    [item setPrice:price];
    [item setInShoppingList:FALSE];
    [item setUuid:[[NSUUID UUID] UUIDString]];
    return item;
}

@end
