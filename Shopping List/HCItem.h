//
//  HCItem.h
//  Shopping List
//
//  Created by Franco Cedillo on 5/27/13.
//  Copyright (c) 2013 HartasCuerdas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HCItem : NSObject <NSCoding>
@property NSString *uuid;
@property NSString *name;
@property float price;
@property BOOL inShoppingList;

+ (HCItem *)createItemWithName:(NSString *)name andPrice:(float)price;

@end
