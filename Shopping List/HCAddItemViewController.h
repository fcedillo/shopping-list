//
//  HCAddItemViewController.h
//  Shopping List
//
//  Created by Franco Cedillo on 5/27/13.
//  Copyright (c) 2013 HartasCuerdas. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol HCAddItemViewControllerDelegate;
@interface HCAddItemViewController : UIViewController
@property (weak) id<HCAddItemViewControllerDelegate> delegate;
@property IBOutlet UITextField *nameTextField;
@property IBOutlet UITextField *priceTextField;
@end

@protocol HCAddItemViewControllerDelegate <NSObject>
- (void)controller:(HCAddItemViewController *)controller didSaveItemWithName:(NSString *)name andPrice:(float)price;
@end
