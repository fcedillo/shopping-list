//
//  HCEditItemViewController.h
//  Shopping List
//
//  Created by Franco Cedillo on 5/30/13.
//  Copyright (c) 2013 HartasCuerdas. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HCItem;
@protocol HCEditItemViewControllerDelegate;
@interface HCEditItemViewController : UIViewController
@property IBOutlet UITextField *nameTextField;
@property IBOutlet UITextField *priceTextField;
#pragma mark -
#pragma mark Initialization
- (id)initWithItem:(HCItem *)item andDelegate:(id<HCEditItemViewControllerDelegate>)delegate;
@end
@protocol HCEditItemViewControllerDelegate <NSObject>
- (void)controller:(HCEditItemViewController *)controller didUpdateItem:(HCItem *)item;
@end
