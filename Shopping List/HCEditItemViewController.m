//
//  HCEditItemViewController.m
//  Shopping List
//
//  Created by Franco Cedillo on 5/30/13.
//  Copyright (c) 2013 HartasCuerdas. All rights reserved.
//

#import "HCEditItemViewController.h"
#import "HCItem.h"

@interface HCEditItemViewController ()
@property HCItem *item;
@property (weak) id<HCEditItemViewControllerDelegate> delegate;
@end

@implementation HCEditItemViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithItem:(HCItem *)item andDelegate:(id<HCEditItemViewControllerDelegate>)delegate {
    self = [super initWithNibName:@"HCEditItemViewController" bundle:nil];
    if (self) {
        // Set Item
        self.item = item;
        // Set Delegate
        self.delegate = delegate;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    // Create a Save Button
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(save:)];
    // Populate Text Fields
    if (self.item) {
        [self.nameTextField setText:[self.item name]];
        [self.priceTextField setText:[NSString stringWithFormat:@"%f", [self.item price]]];
    }
}

- (void)save:(id)sender {
    NSString *name = [self.nameTextField text];
    float price = [[self.priceTextField text] floatValue];
    // Update Item
    [self.item setName:name];
    [self.item setPrice:price];
    // Notify Delegate
    [self.delegate controller:self didUpdateItem:self.item];
    // Pop View Controller
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
